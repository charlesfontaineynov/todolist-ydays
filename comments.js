// Une App est découpée en composant

// Un fichier par composant dans le but de pouvoir les réutiliser et bien découper notre application

// Le State avec le useState qui nous permet de déclarer des variables d'environnement qui sont propre à un composant
// et qui vont le faire réagir

// Les props sont des pasages de variables à des composants enfants
// Les props qui sont des valeurs que l'on passe à un composant enfant pour qu'il puisse utiliser ces valeurs dynamiquement
// Les props sont des paramètres qui arrive en paramètres de notre fonction dans le composant Enfant
// Avec le destructuring d'objet, comme props est un objet, on est capable de récupérer directement la
// propriété d'un objet comme ça :
// {title} = props

// Life Cycle
// Cycle de vie d'un composant

// Chaque composant va passer par 3 états différents : componentDidMount, componentDidUpdate et componentWillUnmount

// componentDidMount
// Quand le composant est appelé pour la 1ère fois, on dit que le composant est monté
// Passe par le return directment pour retourner une valeur
// React se charge de mettre à jour le DOM et les refs

// En fait sur chaque phase, on va pouvoir répondre à des évènements pour pouvoir réagir à certains moments
// du cycle de vie du composant